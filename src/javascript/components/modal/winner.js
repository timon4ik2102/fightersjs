import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  // call showModal function
  showModal({
    title: `Today winner is ${fighter.name}`,
    bodyElement: createFighterImage(fighter),
    onClose: () => {
      location.reload();
    },
  });
}
