import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    let pressedKeys = new Set();

    const leftFighter = Object.assign(firstFighter, {
      startHealth: firstFighter.health,
      criticalHitTime: new Date() - 10001,
    });
    const rightFighter = Object.assign(secondFighter, {
      startHealth: secondFighter.health,
      criticalHitTime: new Date() - 10001,
    });

    const leftHealthIndicator = document.querySelector('#left-fighter-indicator');
    const rightHealthIndicator = document.querySelector('#right-fighter-indicator');

    document.addEventListener('keydown', (event) => {
      pressedKeys.add(event.code);
      if (
        event.code === controls.PlayerOneAttack &&
        !pressedKeys.has(controls.PlayerTwoBlock) &&
        !pressedKeys.has(controls.PlayerOneBlock)
      ) {
        rightFighter.health -= getDamage(leftFighter, rightFighter);
        console.log(rightFighter.health);
        percentHealthIndicator(rightFighter, rightHealthIndicator);
      }

      if (
        event.code === controls.PlayerTwoAttack &&
        !pressedKeys.has(controls.PlayerOneBlock) &&
        !pressedKeys.has(controls.PlayerTwoBlock)
      ) {
        leftFighter.health -= getDamage(rightFighter, leftFighter);
        percentHealthIndicator(leftFighter, leftHealthIndicator);
      }

      if (controls.PlayerOneCriticalHitCombination.every((key) => pressedKeys.has(key))) {
        doneCriticalDamage(leftFighter, rightFighter);
        percentHealthIndicator(rightFighter, rightHealthIndicator);
      }

      if (controls.PlayerTwoCriticalHitCombination.every((key) => pressedKeys.has(key))) {
        doneCriticalDamage(rightFighter, leftFighter);
        percentHealthIndicator(leftFighter, leftHealthIndicator);
      }
    });

    document.addEventListener('keyup', (event) => {
      pressedKeys.delete(event.code);
      if (rightFighter.health <= 0) {
        resolve(firstFighter);
      }
      if (leftFighter.health <= 0) {
        resolve(secondFighter);
      }
    });
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  if (damage > 0) return damage;
  else return 0;
}

export function getHitPower(fighter) {
  // return hit power
  return fighter.attack * (Math.random() + 1);
}

export function getBlockPower(fighter) {
  // return block power
  return fighter.defense * (Math.random() + 1);
}

export function getCriticalHit(fighter) {
  // return critical damage
  return fighter.attack * 2;
}

export function doneCriticalDamage(attacker, defender) {
  const currentTime = new Date();
  const timeLimit = 10000;
  if (currentTime - attacker.criticalHitTime > timeLimit) {
    defender.health -= getCriticalHit(attacker);
    attacker.criticalHitTime = currentTime;
  }
}

export function percentHealthIndicator(defender, healthIndicator) {
  if (defender.health > 0) {
    healthIndicator.style.width = `${Math.round((defender.health / defender.startHealth) * 100)}%`;
  } else healthIndicator.style.width = 0;
}
